const express = require("express");
const app = express();
const path = require("path");
const multer = require("multer");
const logger = require("morgan");
const serveIndex = require("serve-index");

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/uploads");
  },
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  },
});

//will be using this for uplading
const upload = multer({
  storage: storage,
});

app.use(logger("tiny"));
app.use(express.json());
app.use(
  express.urlencoded({
    extended: false,
  })
);

app.use(
  "/files",
  express.static("public"),
  serveIndex("public", {
    icons: true,
  })
);

app.post("/upload", upload.single("file"), function (req, res) {
  console.log("storage location is ", req.hostname + "/" + req.file.path);
  return res.send(req.file);
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log("Server is up and running on port ", port);
});
